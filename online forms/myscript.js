//ADMINISTRATION TAB
var adminbase = [];

//function that creates a new element
var elNumber = 1;
function newElement() {
    var form = document.getElementById("adminbody");
    var element = document.createElement("div");
    element.id = elNumber;
    element.className = "formEl";
    element.innerHTML += "Element " + elNumber + ": ";
    var field = document.createElement("input");      //field for element name entry
    field.type = "text";
    field.className = "field";
    element.appendChild(field);
    var selection1 = document.createElement("select");      //dropdown for input type
    selection1.className = "inputtype";
    selection1.setAttribute("onchange", "rbNumber(this," + element.id + ")");
    optionSelection1(selection1);
    element.appendChild(selection1);
    var radio = document.createElement("select");          //dropdown that appears when radiobutton is selected in previous dropdown
    radio.className = "rbNumber";
    radio.setAttribute("onchange", "createRbLabels(this," + element.id + ")");
    createRbOptions(radio);
    element.appendChild(radio);
    var selection2 = document.createElement("select");    //dropdown for requirement
    selection2.className = "requirement";
    optionSelection2(selection2);
    element.appendChild(selection2);

    form.appendChild(element);

    if (elNumber != 1) {
        var prevEl = document.getElementById("addButton");
        prevEl.parentNode.removeChild(prevEl);                 //remove Add button from previous element
    }
    var addButton = document.createElement("input");
    addButton.id = "addButton";
    addButton.type = "submit";
    addButton.value = "Add";
    addButton.onclick = newElement;
    element.appendChild(addButton);

    var optionsPlaceholder = document.createElement("div");
    optionsPlaceholder.id = "optionsPlaceholder" + elNumber;
    optionsPlaceholder.className = "optionsPlaceholder";
    // optionsPlaceholder.style.display = "none";
    element.appendChild(optionsPlaceholder);

    elNumber++;
}


var options;
//selection function
function optionSelection(element) {
    for (var i = 0; i < options.length; i++) {
        var option = document.createElement("option");
        option.value = options[i];
        option.text = options[i];
        element.appendChild(option);
    }
}
//function for inputtype select
function optionSelection1(element) {
    options = ["Textbox", "Checkbox", "Radiobuttons"];
    optionSelection(element);
}
//function for requirement select
function optionSelection2(element) {
    options = ["None", "Mandatory", "Numeric"];
    optionSelection(element);
}


function create() {
    var crt = document.getElementById("adminbody");
    crt.innerHTML = "";
    crt.style.display = "inline";
    document.getElementById("saveButton").style.display = "inline";
    var button = document.getElementById("button");
}
//creates new form
function createform(fname) {
    create();
    button.setAttribute("onClick", "saveForm(formname.value,'new')");
    elNumber = 1;
    newElement();
}
// function that searches the administration and finds a form (for update) or creates a new one
function search(fname) {
    //find already created template
    var i;
    for (i = 0; i < adminbase.length; i++) {
        if (adminbase[i][0].name == fname) {
            updateform(adminbase[i]);
            break;
        }
    }
    //if the searched form does not exist create it
    if (i == adminbase.length) {
        createform(fname);
    }
}
//function for updating forms 
function updateform(form) {
    create();
    button.setAttribute("onClick", "saveForm(formname.value,'old')");
    for (var i = 1; i < form.length; i++) {
        elNumber = i;
        newElement();
    }
    var l = 0;
    for (var j = 1; j < form.length; j++) {
        var el = document.getElementsByClassName('field');
        el[j - 1].value = form[j].field;
        var input = document.getElementsByClassName('inputtype');
        selected(input[j - 1], form[j].sel);
        if (input == "Radiobutton") {
            var rblabel = document.getElementsByClassName('rblabel');
            for (var k = 0; k < form[j].radio.length; k++) {
                rblabel[k + l].value = form[j].radio[k];
            }
            l = l + form[j].radio.length;
        }
        var req = document.getElementsByClassName('requirement');
        selected(req[j - 1], form[j].requirement);
    }

}

//select function
function selected(selectEl, sel) {
    var opts = selectEl.options;
    for (var option, j = 0; option = opts[j]; j++) {
        if (option.value == sel) {
            selectEl.selectedIndex = j;
            break;
        }
    }
}
//function that creates labels for radiobuttons
function createRbLabels(rbLabels, id) {
    var k = id - 1;
    var op = document.getElementsByClassName("optionsPlaceholder");
    op[k].innerHTML = "";
    op[k].style.display = "block";
    for (var i = 1; i <= rbLabels.value; i++) {
        var rblabel = document.createElement("input");
        rblabel.type = "text";
        rblabel.className = "rblabel";
        op[k].appendChild(rblabel);
        var br = document.createElement("br");
        op[k].appendChild(br);
    }
}
//function that creates number of options(labels) for radiobuttons
function createRbOptions(element) {
    for (var i = 0; i <= 10; i++) {
        var option = document.createElement("option");
        option.value = i;
        option.text = "" + i;
        element.appendChild(option);
    }
}
//function that creates a dropdown list to select the number of radiobuttons and turns off the numeric option for the checkbox and radiobuttons
function rbNumber(sel, id) {
    var rbn = document.getElementsByClassName("rbNumber");
    var op = document.getElementsByClassName("optionsPlaceholder");
    if (sel.value == "Radiobuttons") {
        rbn[id - 1].style.display = "inline";
    }
    else {
        rbn[id - 1].style.display = "none";
        op[id - 1].style.display = "none";
    }
    //radiobuttons and checkbox can't be numeric
    var requirement = document.getElementsByClassName("requirement");
    if (sel.value != "Textbox") {
        requirement[id - 1].options[2].setAttribute("disabled", true);
    }
    else {
        requirement[id - 1].options[2].removeAttribute("disabled");
    }
}

//function that saves formular to adminbase
function saveForm(fname, checkIfOldOrNew) {
    var field = document.getElementsByClassName("field");
    var sel = document.getElementsByClassName("inputtype");
    var radio = document.getElementsByClassName("rbNumber");
    var rblabel = document.getElementsByClassName("rblabel");
    var requirement = document.getElementsByClassName("requirement");

    var template = [
        { id: 0, name: fname }
    ];
    var num = 0;
    var formular = { id: 1, field: "", sel: "", requirement: "", radio: null };
    for (var i = 1; i < elNumber; i++) {
        template[i] = Object.create(formular);
        template[i].id = i;
        template[i].field = field[i - 1].value;
        template[i].sel = sel[i - 1][sel[i - 1].selectedIndex].value;
        template[i].requirement = requirement[i - 1][requirement[i - 1].selectedIndex].value;
        if (sel[i - 1].value == "Radiobuttons") {
            var rbLabelValue = [];
            var val = radio[i - 1][radio[i - 1].selectedIndex].value;
            for (var j = 0; j < val; j++) {
                rbLabelValue[j] = rblabel[j + num].value;
            }
            num += Number(val);
            template[i].radio = rbLabelValue;
        }
        template[i].requirement = requirement[i - 1][requirement[i - 1].selectedIndex].value;
    }
    if (checkIfOldOrNew == "old") {
        for (var l = 0; l < adminbase.length; l++) {
            if (adminbase[l][0].name == fname) {
                adminbase[l] = template;
                alert("Template updated");
                break;
            }
        }
    }
    else if (checkIfOldOrNew == "new") {
        adminbase.push(template);
        alert("Template saved");
    }
}

//FORM TAB

var formbase = [];
//function that allows the selection of created forms 
function formSelection(fname) {
    var selectForm = document.getElementById("savedForm");
    var i;
    for (i = (fname - 1); i < adminbase.length; i++) {
        var option = document.createElement("option");
        option.text = adminbase[i][0].name;
        option.value = adminbase[i][0].name;
        selectForm.appendChild(option);
    }
}
//function to display a form to fill out or to display an already completed form
function showForm(fname, version) {
    var form;
    for (var j = 0; j < adminbase.length; j++) {
        if (adminbase[j][0].name == fname) {
            form = adminbase[j];
            break;
        }
    }
    document.getElementById("formbody").style.display = "inline";
    document.getElementById("forms").innerHTML = "";
    var i;
    for (i = 0; i < formbase.length; i++) {
        if (formbase[i][0].name == fname && formbase[i][0].version == version) {
            filledForm(form, formbase[i]); //show already filled form
            break;
        }
    }
    if (i == formbase.length) {
        for (var i = 1; i < form.length; i++) {
            formLayout(form[i], i); //show a form to fill out
        }
    }
    var butt = document.getElementById("forms");
    var fsaveButton = document.createElement("input");
    fsaveButton.type = "submit";
    fsaveButton.id = "fsaveButton";
    fsaveButton.value = "Save";
    butt.appendChild(fsaveButton);
}
//shape layout function
function formLayout(form, element) {
    var formularf = document.getElementById("forms");
    var section = document.createElement("section");
    section.id = element;
    section.className = "section";
    var field = form.field;

    if (form.requirement == "Mandatory") {
        field += "* : ";
    }
    else {
        field += " : "
    }
    section.innerHTML += field;
    var input = document.createElement("input");
    input.id = form.field;
    if (form.sel == "Textbox") {
        ifNumeric(form, input);        //only textbox can be numeric
        ifMandatory(form, input);
        section.appendChild(input);
    }
    else if (form.sel == "Checkbox") {
        input.type = "checkbox";
        ifMandatory(form, input);
        section.appendChild(input);
    }
    else {
        var sec = document.createElement("div");
        sec.className = "fradiobuttons";
        var radio = [];
        var label = [];
        for (var i = 0; i < form.radio.length; i++) {
            radio[i] = document.createElement("input");
            label[i] = document.createElement("label");
            radio[i].type = "radio";
            radio[i].name = "radio" + element;
            radio[i].id = form.radio[i];
            ifMandatory(form, radio[i]);
            label[i].innerHTML = form.radio[i];
            sec.appendChild(radio[i]);
            sec.appendChild(label[i]);
            sec.appendChild(document.createElement("br"));
        }
        section.appendChild(sec);
    }
    formularf.appendChild(section);
    formularf.appendChild(document.createElement("br"));

}

//function that checks whether the inputtype is mandatory
function ifMandatory(form, input) {
    if (form.requirement == "Mandatory") {
        input.setAttribute("required", true);
    }
}

//function that checks if the inputtype is numeric, if not then it is textual
function ifNumeric(form, input) {
    if (form.requirement == "Numeric") {
        input.type = "number";
        input.setAttribute("placeholder", "Enter a number");
    }
    else {
        input.type = "text";
    }
}

//saving filled formular to formbase
function saveFilledForm(fname, version) {
    var fform = [
        { id: 0, name: fname, version: Number(version) }
    ];
    var formular = { id: 1, Field: "" };

    var template;
    for (var j = 0; j < adminbase.length; j++) {
        if (adminbase[j][0].name == fname) {
            template = adminbase[j];
            break;
        }
    }

    for (var i = 1; i < template.length; i++) {
        fform[i] = Object.create(formular);
        fform[i].id = i;
        if (template[i].sel == "Textbox") {
            var input = document.getElementById(template[i].field);
            fform[i].Field = input.value;
        }
        else if (template[i].sel == "Checkbox") {
            var input = document.getElementById(template[i].field);
            if (input.checked)
                fform[i].Field = true;
            else
                fform[i].Field = false;
        }
        else {
            var input = document.forms["radio" + i];
            for (var j = 0; j < input.length; j++) {
                if (input[j].checked) {
                    fform[i].Field = input[j].id;
                    break;
                }
            }
        }
    }

    formbase.push(fform);
    alert("Formular saved");
}

//function to display an already filled form
function filledForm(form, exist) {
    for (var i = 1; i < form.length; i++) {
        formLayout(form[i], i);
    }
    for (var j = 1; j < exist.length; j++) {
        if (form[j].sel == "Textbox") {
            var input = document.getElementById(form[j].field);
            input.value = exist[j].Field;
        }
        else if (form[j].sel == "Checkbox") {
            var input = document.getElementById(form[j].field);
            if (exist[j].Field) {
                input.setAttribute("checked", true);
            }
        }
        else {
            var input = document.getElementById(exist[j].Field);
            input.setAttribute("checked", true);

        }
    }
}
